import numpy as np
import matplotlib.pyplot as plt
class Viewport:
    ray_iter=0    
    def __init__(self,eye,xsize,ysize):
        self.img= np.zeros((ysize,xsize),np.int32)
        self.x_size=xsize
        self.y_size=ysize
        self.eypos=eye
        self.rays=self.x_size*self.y_size 
    def __str__(self):
        out="Size:"+str(self.x_size)+","+str(self.y_size)+" Eye pos:"+str(self.eypos)
        return out
    def printInfo(self):
        print "*******************************"
        print "Eye position %s" %(self.eypos)
        print "Plane size (%d,%d)" %(self.x_size,self.y_size)
        print "*******************************"
    def showImage(self):
        plt.imshow(self.img)
        plt.show()
    def numberOfRays(self):
        return self.rays
    def __iter__(self):
        self.ray_iter=0;
        return self
    def next(self):
        if self.ray_iter>=self.rays:
             raise StopIteration
        else:
            self.ray_iter+=1
            return self.ray_iter
        
